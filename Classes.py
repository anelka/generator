from orm import Entity

class Category(Entity):
    _pk = "id"
    _fields = ['title']
    _parents = []
    _children = ['articles']
    _siblings = []
    _extensible = []
    _extension = ['user']
    _required_fields = ['title']

class Article(Entity):
    _pk = "id"
    _fields = ['title','context','author']
    _parents = ['category']
    _children = []
    _siblings = ['tags']
    _extensible = []
    _extension = []
    _required_fields = ['title','author','category']

class Tag(Entity):
    _pk = "id"
    _fields = ['value']
    _parents = []
    _children = []
    _siblings = ['articles']
    _extensible = []
    _extension = []
    _required_fields = ['value']

class User(Entity):
    _pk = "email"
    _fields = ['login','password','email']
    _parents = []
    _children = []
    _siblings = []
    _extensible = ['category']
    _extension = []
    _required_fields = ['login','email','password']
