import itertools
import yaml

from Constant import Constant

class Generator():
    def load(self, path):
        with open(path, 'r') as f:
            inputs = yaml.load(f)
        self.inputs = inputs
    
    def dump(self, path):
        with open(path, 'w') as result:
            result.write(self.statement)
    
    def get_pk(self, essence):
        try:
            pk = self.inputs[essence]['primary_key']
            return {'pk': pk,
                    'type_pk': self.inputs[essence]['fields'][pk]}
        except KeyError:
            return {'pk': 'id', 'type_pk': 'INTEGER'}
    
    def is_required_field(self, essence, field):
        try:
            return field in self.inputs[essence]['required_fields']
        except KeyError:
            return False
    
    def is_unique_field(self, essence, field):
        try:
            return field in self.inputs[essence]['unique']
        except KeyError:
            return False
    
    def get_constraint(self, essence, field):
        constraint = []
        if self.is_required_field(essence, field):
            constraint.append('NOT NULL')
        
        if self.is_unique_field(essence, field):
            constraint.append('UNIQUE')
        
        return ' '.join(constraint)
    
    def table(self, essence):
        title = essence.lower()
        fields_list = []
        
        for field, value in self.inputs[essence]['fields'].items():
            constraint = self.get_constraint(essence, field)
            fields_list.append(Constant.FIELD % {'table_name': title,
                                                'field': field,
                                                'type': value,
                                                'constraint': constraint})
        fields_list.append(Constant.CREATE_TIMESTAMP % {'table_name': title})
        fields_list.append(Constant.UPD_TIMESTAMP % {'table_name': title})
        pk = self.get_pk(essence)['pk']
        if pk == 'id':
            fields_list.insert(0, Constant.PK % {'table_name': title})
        else:
            fields_list.append(Constant.FOREIGN_PK % {'table_name': title,
                                                      'field': pk})
        
        return {'table_name': title, 'fields' : ',\n    '.join(fields_list)}
        
    def create_statement(self):
        table_set = set()
        relation_table_set = set()
        constraint_set = set()
        relation_set = set()
        function_set = set()
        trigger_set = set()
        
        for essence in self.inputs:
            args_dict = self.table(essence)
            table_set.add(Constant.TABLE % args_dict)
            function_set.add(Constant.UPD_FUNCTION % args_dict)
            trigger_set.add(Constant.UPD_TRIGGER % args_dict)
            
            try:
                for relation, conditions in self.inputs[essence]['relations'].items():
                    title = essence.lower()
                    relation_title = relation.lower()
                    local_type = conditions['local_type']
                    foreign_type = conditions['foreign_type']
                    pk, type_pk = self.get_pk(relation).values()
                    args = {'table1': title,
                            'table2': relation_title,
                            'type': ' '.join([type_pk,
                                        self.get_constraint(essence, relation_title)]),
                            'pk': pk,
                            'on_delete': self.is_required_field(essence, relation_title) and
                                        'RESTRICT' or 'SET NULL'}
                    if local_type == 'one' and foreign_type == 'many':
                        relation_set.add(Constant.ADD_FK % args)
                    elif (local_type == 'one' and foreign_type == 'one' and
                            conditions['reference_from'] ==  title):
                        args['type'] += 'UNIQUE'
                        relation_set.add(Constant.ADD_FK % args)
                    elif (local_type == 'many' and foreign_type == 'many' and
                            title < relation_title):
                        relation_table_name = '%s_%s' % (title, relation_title)
                        pk1, type1 = self.get_pk(essence).values()
                        pk2, type2 = self.get_pk(relation).values()
                        rel_fields = Constant.RELATION_FIELDS % {'table1': title,
                                                                 'type1': type1,
                                                                 'table2': relation_title,
                                                                 'type2': type2}
                        relation_table_set.add(Constant.TABLE % {
                                                'table_name': relation_table_name,
                                                'fields': rel_fields})
                        relation_set.add(Constant.ADD_RELATION_FK % {
                                                'table1': relation_table_name,
                                                'table2': relation_title,
                                                'pk': pk2})
                        relation_set.add(Constant.ADD_RELATION_FK % {
                                                'table1': relation_table_name,
                                                'table2': title,
                                                'pk': pk1})
            except KeyError: pass
            
            try: 
                hash_fields = self.inputs[essence]['hashed'] 
                function_set.add(Constant.EXTENSION)
                for field in hash_fields:
                    args = {'table_name': essence.lower(),
                            'field': field}
                    function_set.add(Constant.HASH_FUNCTION % args) 
                    trigger_set.add(Constant.HASH_TRIGGER % args)
            except KeyError: pass       
        
        self.statement = '\n'.join(itertools.chain(
            table_set, relation_table_set,
            constraint_set, relation_set,
            function_set, trigger_set
            ))
    
    def create_classes(self):
        result = [Constant.IMPORT]
        
        for essence in self.inputs:
            fields = ('\'%s\'' % field for field in self.inputs[essence]['fields'])
            try:
                relations = self.inputs[essence]['relations']
                parents = ('\'%s\'' % field.lower()
                    for field in relations if relations[field]['local_type'] == 'one' and
                    relations[field]['foreign_type'] == 'many')
                children = ('\'%ss\'' % field.lower()
                    for field in relations if relations[field]['local_type'] == 'many' and
                    relations[field]['foreign_type'] == 'one')
                siblings = ('\'%ss\'' % field.lower()
                    for field in relations if relations[field]['local_type'] == 'many' and
                    relations[field]['foreign_type'] == 'many')
                extensible = ('\'%s\'' % field.lower()
                    for field in relations if relations[field]['local_type'] == 'one' and
                    relations[field]['foreign_type'] == 'one' and
                    relations[field]['reference_from'] == essence.lower())
                extension = ('\'%s\'' % field.lower()
                    for field in relations if relations[field]['local_type'] == 'one' and
                    relations[field]['foreign_type'] == 'one' and
                    relations[field]['reference_from'] == field.lower())
            except KeyError:
                parents = children = siblings = extensible = extension = []
                
            try:
                required = ['\'%s\'' % field
                    for field in self.inputs[essence]['required_fields']]
            except KeyError:
                required = []
                
            result.append(Constant.CLASS % {'table_name': essence,
                        'fields': ','.join(fields),
                        'pk': self.get_pk(essence)['pk'],
                        'parents': ','.join(parents),
                        'children': ','.join(children),
                        'siblings': ','.join(siblings),
                        'extensible': ','.join(extensible),
                        'extension': ','.join(extension),
                        'required': ','.join(required)})
        
        self.statement = '\n'.join(result)
           
if __name__ == '__main__':    
    generator = Generator()
    generator.load('Schema.yaml')
    generator.create_statement()
    generator.dump('Statements.sql')
    generator.create_classes()
    generator.dump('Classes.py')

     

