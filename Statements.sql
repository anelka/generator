CREATE TABLE IF NOT EXISTS "article" (
    "article_id" SERIAL PRIMARY KEY,
    "article_title" varchar(50) NOT NULL UNIQUE,
    "article_context" text ,
    "article_author" varchar(50) NOT NULL,
    "article_created_at" TIMESTAMP DEFAULT NOW(),
    "article_updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "tag" (
    "tag_id" SERIAL PRIMARY KEY,
    "tag_value" varchar(50) NOT NULL UNIQUE,
    "tag_created_at" TIMESTAMP DEFAULT NOW(),
    "tag_updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "user" (
    "user_login" varchar(255) NOT NULL,
    "user_password" varchar(255) NOT NULL,
    "user_email" varchar(255) NOT NULL UNIQUE,
    "user_created_at" TIMESTAMP DEFAULT NOW(),
    "user_updated_at" TIMESTAMP,
    CONSTRAINT "user_pk" PRIMARY KEY ("user_email")
);

CREATE TABLE IF NOT EXISTS "category" (
    "category_id" SERIAL PRIMARY KEY,
    "category_title" varchar(50) NOT NULL UNIQUE,
    "category_created_at" TIMESTAMP DEFAULT NOW(),
    "category_updated_at" TIMESTAMP
);

CREATE TABLE IF NOT EXISTS "article_tag" (
    "article_pk" INTEGER NOT NULL,
    "tag_pk" INTEGER NOT NULL,
    PRIMARY KEY ("article_pk", "tag_pk")
);

ALTER TABLE "user" ADD "category_pk" INTEGER UNIQUE,
  ADD CONSTRAINT "user_category_fk" FOREIGN KEY ("category_pk") REFERENCES "category"("category_id") ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE "article_tag"
  ADD CONSTRAINT "article_tag_article_fk" FOREIGN KEY ("article_pk") REFERENCES "article"("article_id") ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE "article" ADD "category_pk" INTEGER NOT NULL,
  ADD CONSTRAINT "article_category_fk" FOREIGN KEY ("category_pk") REFERENCES "category"("category_id") ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE "article_tag"
  ADD CONSTRAINT "article_tag_tag_fk" FOREIGN KEY ("tag_pk") REFERENCES "tag"("tag_id") ON DELETE CASCADE ON UPDATE CASCADE;

CREATE OR REPLACE FUNCTION updated_category()
RETURNS trigger as $$
    BEGIN
        new.category_updated_at = now();
        return new;
    END;
$$LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION updated_user()
RETURNS trigger as $$
    BEGIN
        new.user_updated_at = now();
        return new;
    END;
$$LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION updated_article()
RETURNS trigger as $$
    BEGIN
        new.article_updated_at = now();
        return new;
    END;
$$LANGUAGE plpgsql;

CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE OR REPLACE FUNCTION updated_tag()
RETURNS trigger as $$
    BEGIN
        new.tag_updated_at = now();
        return new;
    END;
$$LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION hash_user_password()
RETURNS trigger AS $$
    BEGIN
        new.user_password = digest(new.user_password, 'sha256');
        return new;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER "update_category" BEFORE UPDATE ON "category" FOR EACH ROW EXECUTE PROCEDURE updated_category();
CREATE TRIGGER "update_article" BEFORE UPDATE ON "article" FOR EACH ROW EXECUTE PROCEDURE updated_article();
CREATE TRIGGER "update_user" BEFORE UPDATE ON "user" FOR EACH ROW EXECUTE PROCEDURE updated_user();
CREATE TRIGGER user_password_hash BEFORE INSERT OR UPDATE ON "user" FOR EACH ROW EXECUTE PROCEDURE hash_user_password();
CREATE TRIGGER "update_tag" BEFORE UPDATE ON "tag" FOR EACH ROW EXECUTE PROCEDURE updated_tag();