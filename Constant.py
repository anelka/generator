
class Constant():
    TABLE = 'CREATE TABLE IF NOT EXISTS "%(table_name)s" (\n    %(fields)s\n);\n'
    
    PK = '"%(table_name)s_id" SERIAL PRIMARY KEY'
    
    FOREIGN_PK = 'CONSTRAINT "%(table_name)s_pk" PRIMARY KEY ("%(table_name)s_%(field)s")'
    
    FIELD = '"%(table_name)s_%(field)s" %(type)s %(constraint)s'
    
    CREATE_TIMESTAMP = '"%(table_name)s_created_at" TIMESTAMP DEFAULT NOW()'
    
    UPD_TIMESTAMP = '"%(table_name)s_updated_at" TIMESTAMP'
    
    RELATION_FIELDS = ('"%(table1)s_pk" %(type1)s NOT NULL,\n'
                       '    "%(table2)s_pk" %(type2)s NOT NULL,\n'
                       '    PRIMARY KEY ("%(table1)s_pk", "%(table2)s_pk")')
    
    UPD_FUNCTION = ('CREATE OR REPLACE FUNCTION updated_%(table_name)s()\n'
                    'RETURNS trigger as $$\n'
                    '    BEGIN\n'
                    '        new.%(table_name)s_updated_at = now();\n'
                    '        return new;\n'
                    '    END;\n'
                    '$$LANGUAGE plpgsql;\n')
    
    HASH_FUNCTION = ('CREATE OR REPLACE FUNCTION hash_%(table_name)s_%(field)s()\n'
                     'RETURNS trigger AS $$\n'
                     '    BEGIN\n'
                     "        new.%(table_name)s_%(field)s = digest(new.%(table_name)s_%(field)s, 'sha256');\n"
                     '        return new;\n'
                     '    END;\n'
                     '$$ LANGUAGE plpgsql;\n')
    
    UPD_TRIGGER = 'CREATE TRIGGER "update_%(table_name)s" BEFORE UPDATE ON "%(table_name)s" FOR EACH ROW EXECUTE PROCEDURE updated_%(table_name)s();'
    
    HASH_TRIGGER = 'CREATE TRIGGER %(table_name)s_%(field)s_hash BEFORE INSERT OR UPDATE ON "%(table_name)s" FOR EACH ROW EXECUTE PROCEDURE hash_%(table_name)s_%(field)s();'
    
    ADD_FK = ('ALTER TABLE "%(table1)s" ADD "%(table2)s_pk" %(type)s,\n'
              '  ADD CONSTRAINT "%(table1)s_%(table2)s_fk" FOREIGN KEY ("%(table2)s_pk") REFERENCES "%(table2)s"("%(table2)s_%(pk)s") ON DELETE %(on_delete)s ON UPDATE CASCADE;\n')
    
    ADD_RELATION_FK = ('ALTER TABLE "%(table1)s"\n'
             '  ADD CONSTRAINT "%(table1)s_%(table2)s_fk" FOREIGN KEY ("%(table2)s_pk") REFERENCES "%(table2)s"("%(table2)s_%(pk)s") ON DELETE CASCADE ON UPDATE CASCADE;\n')
    
    IMPORT = 'from orm import Entity\n'
    
    EXTENSION = 'CREATE EXTENSION IF NOT EXISTS pgcrypto;\n'
    
    
    CLASS = ('class %(table_name)s(Entity):\n'
             '    _pk = "%(pk)s"\n'
             '    _fields = [%(fields)s]\n'
             '    _parents = [%(parents)s]\n'
             '    _children = [%(children)s]\n'
             '    _siblings = [%(siblings)s]\n'
             '    _extensible = [%(extensible)s]\n'
             '    _extension = [%(extension)s]\n'
             '    _required_fields = [%(required)s]\n')
    
    SELECT = 'SELECT * FROM "%(table_name1)s" WHERE "%(table_name2)s_%(pk_field)s" = \'%(pk)s\';'
    
    SELECT_ALL = 'SELECT "%(table_name)s_%(pk_field)s" FROM "%(table_name)s";'
    
    SELECT_PR = 'SELECT * FROM "%(table_name1)s" %(joins)s WHERE "%(table_name2)s_%(pk_field)s" in (%(pk_list)s);'
    
    JOIN = 'JOIN "%(join_table)s" ON "%(table)s".%(field1)s="%(join_table)s".%(field2)s'
    
    IS_EXIST = 'SELECT exists(SELECT 1 FROM "%(table_name)s" WHERE "%(table_name)s_%(pk_field)s" = \'%(pk)s\');'
    
    UPDATE = 'UPDATE "%(table_name)s" SET (%(fields)s) = (%(val)s) WHERE "%(table_name)s_%(pk_field)s" = \'%(pk)s\';'
    
    INSERT = 'INSERT INTO "%(table_name)s" (%(fields)s) VALUES (%(val)s) RETURNING "%(table_name)s_%(pk_field)s";'
    
    INSERT_RELATION = 'INSERT INTO "%(con_table)s" ("%(table_name1)s_pk", "%(table_name2)s_pk") VALUES (%(pk1)s, %(pk2)s);'
    
    DELETE = 'DELETE FROM "%(table_name)s" WHERE %(table_name)s_%(pk_field)s = \'%(pk)s\';'
    
    DELETE_RELATION = 'DELETE FROM "%(con_table)s" WHERE "%(table_name1)s_pk" = %(pk1)s and "%(table_name2)s_pk" = %(pk2)s;'
    
    COUNT_ALL = 'SELECT count(*) FROM "%(table_name)s";'
    
    COUNT = 'SELECT count(*) FROM "%(table_name)s" WHERE "%(table_name)s_%(field)s" = \'%(field_value)s\';'
    
    FILTER = 'SELECT "%(table_name)s_%(pk_field)s" FROM "%(table_name)s" WHERE %(condition)s;'
    
    FILTER_CONDITIONS = {'start': '%(table)s_%(field)s LIKE \'%(filter)s%(per)s\'',
                        'contains': '%(table)s_%(field)s LIKE \'%(per)s%(filter)s%(per)s\'',
                        'end': '%(table)s_%(field)s LIKE \'%(per)s%(filter)s\'',
                        'greater': '%s_%s >= %s',
                        'less': '%s_%s <= %s'}
    
    TRANSACTION = ('BEGIN:\n %(commands)s\n COMMIT;')
